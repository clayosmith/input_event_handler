CC = gcc
ALL_FLAGS = -g -Wall

main: main.c handle_devices.c
	${CC} ${ALL_FLAGS} main.c handle_devices.c -o devout

student: student.c
	${CC} ${ALL_FLAGS} student.c -o test

clean:
	rm *\.o
