#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "handle_devices.h"

int main(int argc, char** argv)
{
	system("reset");
	setvbuf(stdout, NULL, _IONBF, BUFSIZ);
	if (argc < 2) {
		show_device_options();
		printf("\n\nAbove is a list of all the devices currently connected to your computer.\n" 
				"Find the event(s) above you want to use and determine the event number associated with it/them.\n"
				"Then use the same command with every event number you want to use seperated by a space.\n"
				"EXAMPLE: ./devout 17 14\n"
				"The above example will print output for events /dev/input/event17 and /dev/input/event14\n\n\n"); 
	} else if (argc == 2) {
		//open and print a single event
		unsigned long long event_num;
		char *end_ptr;
		event_num = strtoull(argv[1], &end_ptr, 10); 
		print_one_event(event_num);
	} else {
	      	//open and print multiple events  
		unsigned long long arg_event_nums[argc - 1];
		char* end_ptr;
		for(int i = 1; i < argc; ++i) {
			arg_event_nums[i-1] = strtoull(argv[i], &end_ptr, 10);
		} 
		print_multiple_events(argc - 1, arg_event_nums);
	}
	return 0;
}
