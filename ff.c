#define _GNU_SOURCE /* for asprintf */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <linux/input.h>

#define BITS_PER_LONG (sizeof(long) * 8)
#define BITS_TO_LONGS(x) \
	(((x) + 8 * sizeof (unsigned long) - 1) / (8 * sizeof (unsigned long)))


int main(int argc, char** argv)
{

	int fd = open("/dev/input/event14", O_RDWR); 


	char name[4096];
       	int return_value = ioctl(fd, EVIOCGNAME(sizeof(name)), name);
	printf("%s\n", name);
	printf("%d\n", return_value);


	unsigned long features[BITS_TO_LONGS(FF_CNT)];
       	return_value = ioctl(fd, EVIOCGBIT(EV_FF, sizeof(features)), features); 
	printf("return of ioctl is: %d\n", return_value);
        printf("features[] holds: ");
	for (int i = 0; i < BITS_TO_LONGS(FF_CNT); ++i) {
		printf("%d ", features[i]);
	}
	puts("");

	int num;
	return_value = ioctl(fd, EVIOCGEFFECTS, &num);

	printf("return_value: %d, num: %d\n", return_value, num);

	return 0;
}
