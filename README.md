# Devout

A Linux specific program to aid students learning how to code by giving them variadic input that fluctuates and is imperfect.

### USAGE
```
./devout
./devout 12
./devout 2 5 25
```

The first line will print the available devices and events the user can have printed to the terminal.
The second line prints /dev/input/event12 to standard out.
The third line prints /dev/input/event2, /dev/input/event5, and /dev/input/event25 to standard out.

The output is always 5 columns where the first column is an integer representing the event number passed in by the user. The second is the time since the beginning of the program. The third number is the type associated with the event. The fourth number is the code associated with the event. In the fifth and final column is the current value.

![How to download and install the program](./images/devout_download_and_install.png)
![Show what devices are available to use and get event info](./images/program_with_no_arguments.png)
![Show what devices are available to use and get event info](./images/output_no_arguments.png)
![Program command for printing a single event](./images/command_for_single_event.png)
![Program output for printing a single event](./images/output_for_single_event.png)
![Program command for printing multiple events](./images/command_for_multiple_arguments.png)
![Program output for printing multiple events](./images/output_for_multiple_arguments.png)
![Program command with a students code](./images/command_with_students_code.png)
![Program output with a students example code](./images/output_for_students_example.png)
