#ifndef USER_DEFS_H
#define USER_DEFS_H

#define FIX char buffer[100]; fgets(buffer, 99, stdin); //needed to fix a problem I cannot figure out with the standard input

//Linux Device Event Types
#define	EV_KEY			1
#define	EV_ABS			3
#define	EV_MSC			4

#ifdef VALVE_STEAM
//codes
#define JOY_LR_ABS 		0
#define JOY_UD_ABS		1
#define R_DPAD_LR_ABS      	3
#define R_DPAD_UD_ABS      	4
#define L_DPAD_LR_ABS		16	
#define L_DPAD_UD_ABS		17
#define R_TRIGGER_ABS		20
#define L_TRIGGER_ABS		21
#define L_DPAD_ENGAGED		289
#define R_DPAD_ENGAGED		290
#define A_BTN			304
#define B_BTN			305
#define X_BTN			307
#define Y_BTN			308
#define L_SHOULDER_BTN		310
#define R_SHOULDER_BTN		311
#define L_TRIGGER_BTN		312
#define R_TRIGGER_BTN		313
#define LESSER_BTN    		314
#define GREATER_BTN    		315
#define STEAM_BTN      		316
#define JOY_BTN			317
#define R_DPAD_BTN		318
#define L_GRIP_BTN     		336
#define R_GRIP_BTN     		337
#define L_DPAD_UP_BTN		544
#define L_DPAD_DOWN_BTN		545
#define L_DPAD_LEFT_BTN		546
#define L_DPAD_RIGHT_BTN       	547
#endif

#ifdef XBOX_360
//codes
#define L_JOY_LR		0
#define L_JOY_UD		1
#define LT			2
#define R_JOY_LR		3
#define R_JOY_UD		4
#define	RT			5
#define	DPAD_LR			16
#define DPAD_UD			17
#define A_BTN			304
#define	B_BTN			305
#define X_BTN			307
#define	Y_BTN			308
#define LB_BTN			310
#define RB_BTN			311
#define LESSER_BTN		314
#define GREATER_BTN		315
#define XBOX_BTN		316
#define L_JOY_BTN		317
#define R_JOY_BTN		318
#define DPAD_LEFT_BTN		704
#define DPAD_RIGHT_BTN		705
#define DPAD_UP_BTN		706
#define DPAD_DOWN_BTN		707
#endif

#ifdef PS4 
//events
#define TOUCHPAD 		1
#define GYRO			2
#define BUTTONS			3
//codes
//TOUCHPAD
#define X_COORD			0
#define Y_COORD			1
#define X_COORD2		53
#define Y_COORD2		54
#define TWO_FINGER		333
#define TWO_FINGER_TOGGLE	47
#define ENGAGED_PER_USE		57
#define TRACK_BUTTON		272
#define TRACK_ENGAGED_TOTAL	325
#define TRACK_ENGAGED_TOTAL2	330
//GYRO
#define MOVE_LR			0
#define MOVE_UD			1
#define MOVE_FB			2
#define TILT_LR			3
#define FACE_UD			4
#define TILT_UD			5
#define TIME_STAMP		5
//BUTTONS
#define TRIANGLE		307
#define CIRCLE			305
#define X_BTN			304
#define SQUARE			308
#define PS			316
#define OPTIONS			315
#define SHARE			314
#define DPAD_UP			17
#define DPAD_DOWN		17
#define DPAD_RIGHT		16
#define DPAD_LEFT		16
#define R1_BTN			311	
#define L1_BTN			310
#define R2_BTN			313
#define L2_BTN			312
#define R2_ABS			5
#define L2_ABS			2
#define R_JOYSTICK_BTN		318
#define R_JOYSTICK_UD_ABS	4
#define R_JOYSTICK_LR_ABS	3
#define L_JOYSTICK_BTN		317
#define L_JOYSTICK_UD_ABS	1
#define L_JOYSTICK_LR_ABS	0
#endif //ps4

#ifdef WII
//events
#define GYRO 			1
#define IR 			2
#define BUTTONS			3
#define NUNCHUK			4
//codes
	//GYRO
#define TILT_LR			3
#define	TILT_UD			4
#define	FACE_UD			5
	//IR
// NA
	//BUTTONS
#define	DPAD_UP_BTN		103
#define	DPAD_LEFT_BTN		105
#define	DPAD_RIGHT_BTN		106
#define	DPAD_DOWN_BTN		108
#define	ONE_BTN			257
#define	TWO_BTN			258
#define	A_BTN			304
#define	B_BTN			305
#define	HOME_BTN		316
#define	PLUS_BTN		407
#define	MINUS_BTN		412
	//NUNHCUK
#define	JOY_LR_ABS		16
#define	JOY_UD_ABS		17
#define Z_BTN			309
#define C_BTN			386
#define TILT_LR_ABS		3
#define TILT_UD_ABS		4
#define FACE_UD_ABS		5
#endif //WII

#endif //header
