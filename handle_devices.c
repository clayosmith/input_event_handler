#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/mman.h>
#include <linux/input.h>

#define BUFSIZE 45
#define PATH_SIZE 26

void show_device_options(void)
{
	FILE* pipe = popen("cat /proc/bus/input/devices | \
				grep \"^N:\\|^H:\\|^$\" | \
				cut -d '=' -f2-",
				"r");
	int character = 0;
	character = getc(pipe);
	while (character != EOF) {
		printf("%c", character);
		character = getc(pipe);
	}
}


void print_one_event(unsigned long long event_num)
{
	struct input_event event;
	int event_size = sizeof(struct input_event); 
	unsigned long start_sec;
	char path_to_event[PATH_SIZE];
	snprintf(path_to_event, PATH_SIZE - 1, "/dev/input/event%lld", event_num);
	int fptr = open(path_to_event, O_RDWR);
	int initial_read = read(fptr, &event, event_size);
	if (initial_read == -1) {
			fprintf(stderr, "PROBLEM READING FILE!\n \
						Either your input number does not correlate to a device file or you do not have permission set up properly to read input files. (Most common)");
		exit(-1);
	}
	char buffer[BUFSIZE+1] = {0};
	start_sec = event.time.tv_sec;
	int string_size = 0;
	while (read(fptr, &event, event_size) != -1) {
		if (event.type == 0) continue;
		string_size = snprintf(buffer, BUFSIZE, "%lld  %lu.%06lu  %d  %3d  %11d\n",
				event_num,
				(unsigned long) event.time.tv_sec - start_sec,
				(unsigned long) event.time.tv_usec,
				event.type,
				event.code,
				event.value); 
		write(STDOUT_FILENO, buffer, string_size);
	}
	fprintf(stderr, "DISCONNECTED\n");
}


void print_multiple_events(int num_events, unsigned long long event_nums[])
{
	//prepare to start printing inputs
	struct input_event event[num_events];
	int event_size = sizeof(struct input_event); 
	//open events and get file pointers
	char path_to_event[PATH_SIZE];
	int event_fptrs[num_events];
	for (int i = 0; i < num_events; ++i) {
		snprintf(path_to_event, PATH_SIZE - 1, "/dev/input/event%lld", event_nums[i]);
		event_fptrs[i] = open(path_to_event, O_RDWR);
		if (event_fptrs[i] == -1) {
			fprintf(stderr, "PROBLEM OPENING FILE: %s\n", path_to_event);
		}
	}
	//make sure there was no error
	int initial_read = read(event_fptrs[0], &event[0], event_size);
	if (initial_read == -1) {
		fprintf(stderr, "PROBLEM READING FILE!\n \
					Either your input number does not correlate to a device file or you do not have permission set up properly to read input files. (Most common)");
		exit(-1);
	}
	//shared memory for all processes to get usec
	unsigned long start_sec = event[0].time.tv_sec;
	unsigned long* shared_usec = mmap(NULL, 4096, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	//create the amount of processes appropriate to the number of events to print
	pid_t pids[num_events];
	for (int i = 0; i < num_events; ++i) {
		pids[i] = -1;
	}
	for (int i = 0; i < num_events - 1; ++i) {
	     	pids[i] = fork();
	       	if (pids[i] == 0) break; 
	}
	//mark the parent
	bool isParent = true;
	for (int i = 0; i < num_events; ++i) {
		if (pids[i] == 0) isParent &= false;
	}
	//find and run code just for the parent process (first event (a.k.a. index 0))
	if (isParent) {
		char buffer[BUFSIZE+1];
		int initial_read = read(event_fptrs[0],  &event[0], event_size);
		if (initial_read == -1) {
			fprintf(stderr, "PROBLEM READING FILE!\n \
						Either your input number does not correlate to a device file or you do not have permission set up properly to read input files. (Most common)");
			exit(-1);
		}
		int string_size = 0;
		while (read(event_fptrs[0], &event[0], event_size) != -1) {
			*shared_usec = event[0].time.tv_usec;
			if (event[0].type == 0) continue;
			string_size = snprintf(buffer, BUFSIZE, "%lld  %lu.%06lu  %d  %3d  %11d\n",
					event_nums[0],
					event[0].time.tv_sec - start_sec,
					event[0].time.tv_usec,
					event[0].type,
					event[0].code,
					event[0].value); 
			write(STDOUT_FILENO, buffer, string_size);
		}
		fprintf(stderr, "DISCONNECTED\n");
	}
	//find the child process(es) and run the correct code for each (event #2 onwards) (a.k.a. index 1+))
	for (int i = 0; i < num_events; ++i) {
		if (pids[i] == 0) {
			//put the rest of the code in here?
			char buffer[BUFSIZE+1];
			int initial_read = read(event_fptrs[i+1], &event[i+1], event_size);
			if (initial_read == -1) {
				fprintf(stderr, "PROBLEM READING FILE!\n \
							Either your input number does not correlate to a device file or you do not have permission set up properly to read input files. (Most common)");
				exit(-1);
			}
			int string_size = 0;
			while (read(event_fptrs[i+1], &event[i+1], event_size) != -1) {
				if (event[i+1].type == 0) continue;
				string_size = snprintf(buffer, BUFSIZE, "%lld  %lu.%06lu  %d  %3d  %11d\n",
						event_nums[i+1],
						event[i+1].time.tv_sec - start_sec,
						event[i+1].time.tv_usec,
						event[i+1].type,
						event[i+1].code,
						event[i+1].value); 
				write(STDOUT_FILENO, buffer, string_size);
			}
			fprintf(stderr, "DISCONNECTED\n");
		}
	}
}
